package handlers

import (
	"encoding/json"
	"net/http"

	"godb/models"
	"godb/repositories"
)

func GetCursos(w http.ResponseWriter, r *http.Request) {
	cursos := repositories.GetCursos()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(cursos)
}

func CrearCurso(w http.ResponseWriter, r *http.Request) {
	var curso models.Curso
	json.NewDecoder(r.Body).Decode(&curso)
	curso = repositories.CrearCurso(curso)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(curso)
}
