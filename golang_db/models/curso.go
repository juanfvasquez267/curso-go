package models

import "gorm.io/gorm"

type Curso struct {
	gorm.Model
	Nombre      string `json:"nombre"`
	Descripcion string `json:"descripcion,omitempty"`
}
