package repositories

import (
	"godb/db"
	"godb/models"
)

func GetCursos() (cursos []models.Curso) {
	db := db.GetDB()
	db.Find(&cursos)
	return
}

func CrearCurso(body models.Curso) (curso models.Curso) {
	db := db.GetDB()
	db.Create(&body)
	curso = body
	return
}
