package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"godb/db"
	"godb/handlers"
	"godb/middlewares"
)

const PORT = ":8000"

func main() {
	db.InitDB()
	router := mux.NewRouter()
	router.HandleFunc("/", middlewares.Middleware(handlers.GetCursos)).Methods("GET")
	router.HandleFunc("/", handlers.CrearCurso).Methods("POST")
	http.Handle("/", router)
	log.Println("Escuchando en el puerto: ", PORT)
	log.Fatal(http.ListenAndServe(PORT, nil))
}
