package main

import "fmt"

func main() {
	var arreglo [5]int
	fmt.Println(arreglo)
	// var arreglo2 [10]int
	// arreglo2 = arreglo -> Error

	var cadenas [10]string
	cadenas[0] = "Hola"
	cadenas[9] = "mundo"
	fmt.Println(cadenas)

	letras := [3]string{"a", "b", "c"}
	letras[0] = "H"
	fmt.Println(letras)
	fmt.Println(len(letras))

	fmt.Println("------------------------------")
	// for i, _ := range letras { -> Guion bajo para escapar variables que no uso
	for i, v := range letras {
		fmt.Printf("Índice: %d - Valor: %s ; ", i, v)
	}
}
