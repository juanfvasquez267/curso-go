package main

import "fmt"

func main() {
	var i int

	// for j := 0; j < 10; j++ {}
	// for ; j < 10 ; {} -> While
	// for ; ; {} -> while (true)

	for {
		if i == 5 {
			i++
			continue
		}
		fmt.Println(i)
		i++
		if i >= 10 {
			break
		}
	}
}
