package main

import "fmt"

func main() {
	x := 30
	y := 20

	// if x = 10; x > y { -> Se puede hacer
	if x > y && x > 3 || y < 10 {
		fmt.Println("x es mayor que y")
	} else {
		fmt.Println("x es menor que y")
	}
}
