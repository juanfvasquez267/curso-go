package main

import "fmt"

type Persona struct {
	nombre string
	edad   int
}

type Mascota struct {
	nombre string
	edad   int
}

func (persona Persona) saludo() {
	fmt.Println("Hola " + persona.nombre)
}

func (persona *Persona) incrementarEdad(cant int) {
	persona.edad += cant
	fmt.Println("Dentro de la función")
	fmt.Println(persona)
}

type Empleado struct {
	Persona // -> Herencia, campo anónimo
	cargo   string
}

func main() {
	persona := Persona{nombre: "Juan", edad: 30}
	fmt.Println(persona)
	persona.nombre = "Otro"
	fmt.Println(persona)
	persona.saludo()
	persona.incrementarEdad(10)
	fmt.Println("Fuera de la función")
	fmt.Println(persona)

	empleado := Empleado{}
	empleado.cargo = "Programador"
	empleado.nombre = "Juan"
	empleado.edad = 30

	empleado.incrementarEdad(10)
}
