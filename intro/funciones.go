package main

import "fmt"

func main() {
	// var nombre string
	// nombre = "Juan"
	// saludar()
	// saludar(nombre)
	// saludar(nombre, "vasquez")

	// suma := suma(10, 15)
	// fmt.Println(suma)
	suma, resta, multiplicacion, division := operaciones(10, 15)
	// suma, resta, multiplicacion, _ := operaciones(10, 15)
	fmt.Println(suma)
	fmt.Println(resta)
	fmt.Println(multiplicacion)
	fmt.Println(division)
}

// func saludar() {
// 	fmt.Println("Hola mundo!")
// }
// func saludar(nombre string) {
// 	fmt.Println("Hola " + nombre)
// }
func saludar(nombre, apellido string) {
	// func saludar(nombre, apellido string, numero1, numero2, numero3 int) {
	fmt.Println("Hola " + nombre + " " + apellido)
}

// func suma(x, y int) int {
// 	return x + y
// }
func suma(x, y int) (suma int) {
	suma = x + y
	return
}

func operaciones(x, y int) (suma, resta, multiplicacion, division int) {
	suma = x + y
	resta = x - y
	multiplicacion = x * y
	division = x / y
	return
}
