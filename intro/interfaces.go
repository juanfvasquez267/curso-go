package main

import "fmt"

type Animal interface {
	Mover()
	Comer(comida string) string
}

type Perro struct {
	nombre string
	patas  int
}

type Serpiente struct {
	nombre  string
	especie string
}

func (perro *Perro) Mover() {
	fmt.Println("Corriendo")
}

func (serpiente *Serpiente) Mover() {
	fmt.Println("Reptando")
}

func (perro *Perro) Comer(comida string) string {
	fmt.Println("Comiendo")
	return "Comió"
}

func cualquiera(animal Animal) {
	animal.Mover()
}

func main() {
	perro := Perro{"Zeus", 4}
	cualquiera(&perro)
	serpiente := Serpiente{"ABC", "aasdfjnk"}
	cualquiera(&serpiente)
}
