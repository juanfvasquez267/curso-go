package main

import "fmt"

func main() {
	mapa := make(map[string]int)
	mapa["uno"] = 1
	mapa["dos"] = 2
	mapa["tres"] = 3
	fmt.Println(mapa)

	// lista := make(map[int]int)
	// lista[0] = 0
	fmt.Println(mapa["tres"])
	mapa["tres"] = 100
	fmt.Println(mapa)

	for k, v := range mapa {
		fmt.Printf("%s -> %d \n", k, v)
	}
}
