package main

import "fmt"

func main() {
	a := 10
	fmt.Println(a)
	fmt.Println(duplicarRef(&a))
	fmt.Println(a)
}

func duplicar(num int) int {
	num *= 2
	return num
}

func duplicarRef(num *int) int {
	*num *= 2
	return *num
}
