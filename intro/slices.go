package main

import "fmt"

func main() {
	// var arreglo [5]int
	// var slice []int
	// fmt.Println(slice)
	// fmt.Println(len(slice))
	// fmt.Println(cap(slice))

	// slice = append(slice, 10, 20, 30)
	// fmt.Println(slice)
	// fmt.Println(len(slice))
	// fmt.Println(cap(slice))

	slice2 := make([]int, 5, 100)
	fmt.Println(slice2)
	fmt.Println(len(slice2))
	fmt.Println(cap(slice2))

	slice3 := []int{1, 2, 3}

	copy(slice3, slice2)
	fmt.Println(slice3)
}
