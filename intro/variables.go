package main

import "fmt"

func main() {
	// numericos int float32 float64
	var numero int // Declarar
	numero = 10    // Inicializar
	fmt.Println(numero)
	// var flotante float32
	flotante := 10.1 // Declarar y inicializar
	fmt.Println(flotante)

	// cadenas string
	// var cadena string
	// cadena = "Hola mundo"
	cadena := "Hola mundo"
	fmt.Println(cadena)

	cadena = "100"

	caracter := "a"
	fmt.Println(caracter)

	// booleanos bool
	var bandera bool
	bandera = true //false
	fmt.Println(bandera)

	suma := numero + int(flotante)
	fmt.Println(suma)

	const pi = 3.14
	fmt.Println(pi)

	// arreglos - slices
	// mapas - maps
	// punteros *
	// structs
}
