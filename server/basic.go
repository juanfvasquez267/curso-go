package main

import (
	"io"
	"log"
	"net/http"
)

// Endpoints

func main() {

	// 200 OK
	// 300 Redirect
	// 400 Bad Request - Client Error
	// 500 Internal Server Error - Server Error
	// API REST - RESTFUL

	http.HandleFunc("/usuarios", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {
			io.WriteString(w, "Servicio de usuarios")
		} else {
			http.NotFound(w, r)
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Enviando a /usuarios")
		http.Redirect(w, r, "/usuarios", 301)
	})

	log.Println("Escuchando en el puerto 8000")
	log.Fatal(http.ListenAndServe(":8000", nil))
}
