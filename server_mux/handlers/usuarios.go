package handlers

import (
	"encoding/json"
	"net/http"

	"goexample/server_mux/models"
)

func ObtenerUsuarios(w http.ResponseWriter, r *http.Request) {
	usuarios := make([]string, 0)
	usuarios = append(usuarios, "Juan")
	usuarios = append(usuarios, "Pedro")
	usuarios = append(usuarios, "Maria")
	json.NewEncoder(w).Encode(usuarios)
}

func CrearUsuario(w http.ResponseWriter, r *http.Request) {
	var usuario models.Usuario
	err := json.NewDecoder(r.Body).Decode(&usuario)
	if err != nil {
		// w.WriteHeader(400)
		http.Error(w, "Error al leer el usuario", http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(usuario)
}
