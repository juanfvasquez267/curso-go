package tests

import "fmt"

func main() {
	fmt.Println(hello())
}

func hello() string {
	return "Hola mundo"
}

func Calculo(x, y int) int {
	if x == 0 || y == 0 {
		return 0
	}
	if x < 0 || y < 0 {
		return x + y
	}
	return x + y
}
