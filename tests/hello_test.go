package tests

import "testing"

func TestHello(t *testing.T) {
	respuesta := hello()
	if respuesta != "Hola mundo" {
		t.Errorf("hello() = %q, want %q", respuesta, "Hola mundo")
	}
}

func TestCalculo(t *testing.T) {
	t.Run("Probar que suma enteros mayores a 0", func(t *testing.T) {
		respuesta := Calculo(1, 2)
		if respuesta != 3 {
			t.Errorf("Calculo(1, 2) = %q, want %q", respuesta, 3)
		}
	})
	t.Run("Probar que pasas cuando son 0", func(t *testing.T) {
		respuesta := Calculo(0, 0)
		if respuesta != 0 {
			t.Errorf("Calculo(0, 0) = %q, want %q", respuesta, 0)
		}
	})
	t.Run("Probar que pasa cuando x o y son negativos", func(t *testing.T) {
		respuesta := Calculo(1, -1)
		if respuesta != -1 {
			t.Errorf("Calculo(1, -1) = %q, want %q", respuesta, -1)
		}
	})
}
